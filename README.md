# Rlyeh ISPConfig

A simple, plain ISPConfig without email, DNS, FTP and all that bloaty stuff.  
Mostly based on [ISPConfig by Till Brehm](https://www.howtoforge.com/tutorial/perfect-server-debian-9-stretch-apache-bind-dovecot-ispconfig-3-1) and [jerob/docker-ispconfig](https://hub.docker.com/r/jerob/docker-ispconfig).

## Requirements

* Docker
* Docker Compose

## Usage

1. Clone.
1. Create `.env` file:

```
printf "%s" "MYSQL_PASSWORD=" > .env
tr -dc 'a-zA-Z0-9' < /dev/urandom | dd bs=60 count=1 iflag=fullblock status=none 2> /dev/null >> .env
printf "\n" >> .env
printf "%s" "MYSQL_ROOT_PASSWORD=" >> .env
tr -dc 'a-zA-Z0-9' < /dev/urandom | dd bs=60 count=1 iflag=fullblock status=none 2> /dev/null >> .env
printf "\n" >> .env
```

1. Check settings: `docker-compose config`. Edit as necessary.
1. Build: `docker-compose build`.
1. Run: `docker-compose up -d`.

ISPConfig listens at 0.0.0.0:8080 (TLS). Webs are at 0.0.0.0:80 (plain). SSH listens at 127.0.0.1:2222.  
Currently, TLS should be managed by a reverse proxy.

**IMPORTANT**: Default ISPConfig credential is `user: admin, password: admin`, so log in and change it immediately.

## License

**Rlyeh ISPConfig** is made by [HacKan](https://hackan.net) under GNU GPL v3.0+. You are free to use, share, modify and share modifications under the terms of that [license](LICENSE). Special thanks to [Jerob](https://github.com/jerob) and Till Brehm.

    Copyright (C) 2017 HacKan (https://hackan.net)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

